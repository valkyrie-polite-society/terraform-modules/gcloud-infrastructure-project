resource "gitlab_project_variable" "service_account" {
  project           = gitlab_project.instance.id
  key               = "GOOGLE_APPLICATION_CREDENTIALS"
  value             = base64decode(google_service_account_key.pipeline.private_key)
  variable_type     = "file"
  protected         = false
  environment_scope = "*"
}

resource "gitlab_project_variable" "file" {
  for_each = var.file_vars

  project           = gitlab_project.instance.id
  key               = each.key
  value             = each.value
  variable_type     = "file"
  protected         = false
  environment_scope = "*"
}

resource "gitlab_project_variable" "environment" {
  for_each = var.env_vars

  project           = gitlab_project.instance.id
  key               = each.key
  value             = each.value
  protected         = false
  environment_scope = "*"
}
