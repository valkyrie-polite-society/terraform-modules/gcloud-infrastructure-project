variable "name" {
  type = string
}

variable "path" {
  type    = string
  default = null
}

variable "namespace_id" {
  type    = number
  default = 0
}

variable "description" {
  type = string
}

variable "visibility_level" {
  default = "public"
}

variable "shared_runners_enabled" {
  default = true
}

variable "default_branch" {
  type    = string
  default = "main"
  validation {
    condition     = var.default_branch != "master"
    error_message = "The default branch may not be named master. See https://inclusivenaming.org/ and https://github.com/github/renaming for alternative names."
  }
}

variable "env_vars" {
  type    = map(any)
  default = {}
}

variable "file_vars" {
  type    = map(any)
  default = {}
}

variable "pipelines" {
  type = list(object({
    description = string,
    ref         = string,
    cron        = string,
    variables   = map(string),
    active      = bool,
  }))

  default = [
    {
      description = "Daily"
      ref         = "main"
      cron        = "12 13 * * *"
      variables   = {}
      active      = true
    }
  ]
}

variable "project" {
  type        = string
  description = "Google project in which to create IAM permissions"
}

variable "pipeline_iam_roles" {
  type    = list(string)
  default = []
}
