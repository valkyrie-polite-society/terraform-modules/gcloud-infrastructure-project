terraform {
  required_version = ">= 0.13.7"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.7.0"
    }
    google = {
      source  = "hashicorp/google"
      version = ">= 3.82.0"
    }
  }
}
